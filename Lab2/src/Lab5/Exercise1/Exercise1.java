package Lab5.Exercise1;

public class Exercise1 {
    public static void main(String[] args) {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(4.5, "blue", false);
        Rectangle rectangle1 = new Rectangle(3, 6, "yellow", true);
        Rectangle rectangle2 = new Rectangle(8, 4, "red", false);
        Square square1 = new Square(5);
        Square square2 = new Square(3, "pink", true);
        Shape[] shapes = {circle1, circle2, rectangle1, rectangle2, square1, square2};
        for(Shape a:shapes){
            System.out.println("Area and perimeter of shape: ");
            System.out.println(a.getArea());
            System.out.println(a.getPerimeter());;
        }
    }
}
