package Lab5.Exercise3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    public int tempSensor;

    @Override
    int ReadValue() {
        Random rand = new Random();
        int upperbound = 101;
        tempSensor = rand.nextInt(upperbound);
        return tempSensor;
    }
}
