package Lab5.Exercise3;

import java.util.Random;

public class LightSensor extends Sensor{
    public int lightSensor;

    @Override
    int ReadValue() {
        Random rand = new Random();
        int upperbound = 101;
        lightSensor = rand.nextInt(upperbound);
        return lightSensor;
    }
}
