package Lab5.Exercise3;

abstract class Sensor {
    private String location;

    abstract int ReadValue();
    public String getLocation() {
        return location;
    }
}
