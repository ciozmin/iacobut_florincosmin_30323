package Lab5.Exercise3;

public class Controller {
    private static volatile Controller controller = null;

    private Controller(){}

    public static Controller getController() {
        synchronized (Controller.class) {
            if (controller == null) {
                controller = new Controller();
            }
            return controller;
        }
    }

    public void control(){
        TemperatureSensor tempSensor = new TemperatureSensor();
        LightSensor lightSensor = new LightSensor();

        System.out.println("\nSENSOR READINGS");
        System.out.println("Temperature sensor = " + tempSensor.ReadValue());
        System.out.println("Light sensor       = " + lightSensor.ReadValue());
    }
}
