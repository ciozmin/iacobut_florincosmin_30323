package Lab5.Exercise3;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Exercise3 {
    public static void main(String[] args) {
        Controller controller = Controller.getController();
        ScheduledExecutorService control = Executors.newScheduledThreadPool(0);
        control.scheduleAtFixedRate(new Runnable() {
            int count = 0;
            @Override
            public void run() {
                count++;
                controller.control();
                if(count == 20)
                    control.shutdown();
            }
        }, 0, 1, TimeUnit.SECONDS);  // execute every x seconds

    }
}
