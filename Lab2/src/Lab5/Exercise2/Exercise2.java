package Lab5.Exercise2;

import java.net.Proxy;

public class Exercise2 {
    public static void main(String[] args) {
        ProxyImage image1 = new ProxyImage("car.jpg", true);
        ProxyImage image2 = new ProxyImage("dog.jpg", false);

        image1.display();
        image2.display();
    }
}