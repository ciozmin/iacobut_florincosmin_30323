package Lab9;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.*;

public class ReadFile implements Runnable, ActionListener {
    JTextField tField = new JTextField();
    JButton btnOpen = new JButton("Open");
    JTextArea tArea = new JTextArea();

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new ReadFile());
    }

    @Override
    public void run() {
        JFrame frame = new JFrame("Read text from file");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.add(createMainPanel(), BorderLayout.CENTER);

        frame.pack();
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    private JPanel createMainPanel() {
        JPanel panel = new JPanel(null);
        //panel.setBorder(BorderFactory.createEmptyBorder(0, 15, 15, 15));
        panel.setPreferredSize(new Dimension(500, 500));

        tField.setBounds(10, 10, 340, 30);
        panel.add(tField);

        btnOpen.setBounds(360, 10, 130, 30);
        panel.add(btnOpen);
        btnOpen.addActionListener(this::actionPerformed);

        tArea.setBounds(10, 50, 480, 440);
        tArea.setEditable(false);
        panel.add(tArea);

        return panel;
    }

    void open(String f)
    {
        try
        {
            tArea.setText("");
            BufferedReader bf =
                    new BufferedReader(
                            new FileReader(new File(f)));
            String l = "";
            l = bf.readLine();
            while (l!=null)
            {
                tArea.append(l+"\n");
                l = bf.readLine();
            }
        }catch(Exception e){}
    }

    public void actionPerformed(ActionEvent e) {
        //I don't know how to make it return the absolute path, regardless of who runs it and from what directory
        //Therefore it only works on my system. Replace with your package directory, add the text file there, and it should work :)
        String fileName = "D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab9\\" + tField.getText();
        open(fileName);
    }

}