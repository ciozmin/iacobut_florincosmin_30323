package Lab3;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    Circle(double radius){
        this.radius = radius;
    }
    Circle(double radius, String color){
        this(radius);
        this.color = color;
    }

    public Circle() {

    }

    String getColor(){
        return color;
    }
    double getRadius(){
        return radius;
    }
    double getArea(){
        return 3.14 * radius * radius;
    }
}
