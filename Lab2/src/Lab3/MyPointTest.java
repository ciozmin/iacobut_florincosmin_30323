package Lab3;

public class MyPointTest {
    public static void main(String[] args){
        MyPoint A = new MyPoint();
        MyPoint B = new MyPoint(5,3);
        MyPoint C = new MyPoint();
        MyPoint D = new MyPoint();

        C.setXY(3,-1);
        D.setX(9);
        D.setY(3);

        System.out.println(A.toString());
        System.out.println(B.toString());
        System.out.println(C.toString());

        System.out.println("The distance between C(5, 3) and a random point(3, 3) is: " + C.distance(3,3));
        System.out.println("The distance between points D and C is: " + D.distance(C));
    }
}
