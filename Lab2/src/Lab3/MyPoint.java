package Lab3;

import java.lang.*;

public class MyPoint {
    int x, y;

    //Constructors
    MyPoint(){
        x = 0;
        y = 0;
    }
    MyPoint(int x, int y){
        this.x = x;
        this.y = y;
    }

    //Methods
    void setX(int x){
        this.x = x;
    }
    int getX(){
        return x;
    }
    void setY(int y){
        this.y = y;
    }
    int getY(){
        return y;
    }
    void setXY(int x, int y){
        this.x = x;
        this.y = y;
    }
    public String toString(){
        return ("( " + x + ", " + y + " )");
    }
    int distance(int x, int y){
        // I don't know if you wanted just the distance calculated and written in (x, y) format or
        // the actual distance formula; otherwise I would have just subtracted (x2-x1) and (y2-y1)
        // and returned the result by defining the distance method of type MyPoint (to return both x and y) like:
        // MyPoint distance(int x, int y){
        //  .....
        //  return someVariableOfTypePoint;}
        return (int) Math.sqrt((x-this.x)*(x-this.x)+(y-this.y)*(y-this.y));
    }
    int distance(MyPoint another){
        return (int) Math.sqrt((another.x-this.x)*(another.x-this.x)+(another.y-this.y)*(another.y-this.y));
    }
}
