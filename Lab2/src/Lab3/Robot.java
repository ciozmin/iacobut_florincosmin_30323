package Lab3;

public class Robot {
    int x;

    Robot(){
        x = 1;
    }

    void change(int k){
        if (k >= 1) x += k;
    }

    public String toString(){
        return ("The robot's current position is: " + x);
    }
}
