package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exercise1 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the first number:");
        int i = Integer.parseInt(br.readLine());
        System.out.print("Enter the second number:");
        int j = Integer.parseInt(br.readLine());
        System.out.println("The bigger number is: " + Math.max(i,j));
    }

}