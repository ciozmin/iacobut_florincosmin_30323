package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exercise3 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int prime = 0, i, j;
        boolean ok = true;
        System.out.print("Enter the first number:");
        int A = Integer.parseInt(br.readLine());
        System.out.print("Enter the second number:");
        int B = Integer.parseInt(br.readLine());
        for (i = A + 1; i < B; i ++)
        {
            for (j = 2; j < i; j ++)
                if(i % j == 0)
                {
                    ok = false;
                    break;
                }
            if (ok == true)
            {
                System.out.print(i + " ");
                prime++;
            }
            ok = true;
        }
        System.out.println();
        System.out.println(prime);
    }
}