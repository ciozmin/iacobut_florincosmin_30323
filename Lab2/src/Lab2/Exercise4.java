package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Exercise4 {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);

        int maxx = -1;
        System.out.print("How many numbers?");
        int n = in.nextInt();
        int[] v = new int[n];
        for (int i = 0; i < n; i ++)
            v[i] = in.nextInt();
        for (int i = 0; i < n; i ++)
            if(v[i] > maxx)
                maxx = v[i];
        System.out.println("The biggest number is :" + maxx);
    }
}