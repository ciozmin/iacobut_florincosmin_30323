package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exercise2b {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter String:");
        String k = "";
        int i = Integer.parseInt(br.readLine());
        switch (i) {
            case 1:
                k = "ONE";
                break;
            case 2:
                k = "TWO";
                break;
            case 3:
                k = "THREE";
                break;
            case 4:
                k = "FOUR";
                break;
            case 5:
                k = "FIVE";
                break;
            case 6:
                k = "SIX";
                break;
            case 7:
                k = "SEVEN";
                break;
            case 8:
                k = "EIGHT";
                break;
            case 9:
                k = "NINE";
                break;
            default:
                k = "OTHER";
                break;
        }
        System.out.println(k);
    }
}