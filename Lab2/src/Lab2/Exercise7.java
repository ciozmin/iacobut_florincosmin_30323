package Lab2;

import java.util.Scanner;
import java.util.Random;

public class Exercise7 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        int upperbound = 10;
        int key = rand.nextInt(upperbound);
        int tries = 3, guess;
        while (tries > 0)
        {
            guess = in.nextInt();
            if (guess == key)
            {
                System.out.println("You won!");
                break;
            }
            else if(guess > key)
            {
                System.out.println("Wrong answer! your number is too high!");
                tries --;
            }
            else if (guess < key)
            {
                System.out.println("Wrong answer! your number is too low!");
                tries --;
            }
        }
        if (tries == 0)
            System.out.println("You lost!");

    }
}