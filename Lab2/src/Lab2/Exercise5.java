package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Exercise5 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] v = new int[10];
        int temp;
        Random rand = new Random();
        int upperbound = 1000;
        for (int i = 0; i < 10; i ++)
            v[i] = rand.nextInt(upperbound);
        for (int i = 0; i < 9; i ++)
            for (int j = i + 1; j < 10; j ++)
                if(v[j] < v[i])
                {
                    temp = v[j];
                    v[j] = v[i];
                    v[i] = temp;
                }
        for(int i = 0; i < 10; i ++)
            System.out.println(v[i]);

    }
}