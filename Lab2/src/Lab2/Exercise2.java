package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exercise2 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter String:");
        String k = "";
        int i = Integer.parseInt(br.readLine());
        if (i == 1) {k = "ONE";}
        else if (i == 2) {k = "TWO";}
        else if (i == 3) {k = "THREE";}
        else if (i == 4) {k = "FOUR";}
        else if (i == 5) {k = "FIVE";}
        else if (i == 6) {k = "SIX";}
        else if (i == 7) {k = "SEVEN";}
        else if (i == 8) {k = "EIGHT";}
        else if (i == 9) {k = "NINE";}
        else k = "OTHER";
        System.out.println(k);
    }
}