package Lab4;

public class AuthorTest {
    public static void main(String[] args){
        Author author = new Author("Radu Miron","radumiron@cs.utcluj.ro", 'M');
        System.out.println("name: " + author.getName());
        System.out.println("email: " + author.getEmail());
        System.out.println("gender: " + author.getGender());
        author.setEmail("radu_miron@cs.utcluj.ro");
        System.out.println(author);
    }
}