package Lab4;

public class CylinderTest {
    public static void main(String[] args){
        Circle cylinder = new Cylinder(4, 5);
        System.out.println("cylinder height: " + ((Cylinder)cylinder).getHeight());
        System.out.println("cylinder volume: " + ((Cylinder)cylinder).getVolume());
        System.out.println("cylinder radius: " + cylinder.getRadius());
        System.out.println("cylinder area: " + cylinder.getArea());
    }
}