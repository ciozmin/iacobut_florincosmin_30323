package Lab4;

public class BookTest {
    public static void main(String[] args){
        Book book1 = new Book("Atomic Habits", new Author("James Clear", "jamesclear@gmail.com", 'M'), 12, 100);
        System.out.println(book1);

        System.out.println("author: " + book1.getAuthor());
        System.out.println("initial price: " + book1.getPrice() + "$");
        book1.setPrice(30.25);
        System.out.println("new price: " + book1.getPrice() + "$");
    }
}
