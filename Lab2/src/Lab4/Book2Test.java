package Lab4;

public class Book2Test {
    public static void main(String[] args){
        Author author1 = new Author("Gary Chartrand", "garychartrand@gmail.com", 'M');
        Author author2 = new Author("Albert Polimeni", "albert_polimeni@gmail.com", 'M');
        Author author3 = new Author("Ping Zhang", "pingzhangi@gmail.com", 'M');
        Author[] authors = {author1, author2, author3};

        Book2 book1 = new Book2("Mathematical Proofs", authors, 20);

        System.out.println(book1);
        book1.printAuthors();
    }
}
