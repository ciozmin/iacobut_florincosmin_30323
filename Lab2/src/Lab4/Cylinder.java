package Lab4;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder(){
        super();
    }
    public Cylinder(double radius){
        super(radius);
    }
    public Cylinder(double radius, double height){
        super(radius);
        this.height = height;
    }
    public double getHeight() {
        return height;
    }
    public double getVolume(){
        return Math.PI * Math.pow(getHeight(), 2) * height;
    }
    @Override
    public double getArea(){
        return 2 * Math.PI * height + (getRadius() + height);
    }
}
