package Lab4.Exercise6;

public class Exercise6 {
    public static void main(String[] args){
        //Shape objects
        Shape triangle = new Shape();
        Shape anotherTriangle = new Shape("blue", false);

        //Circle objects
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.5, "blue", false);

        //Rectangle objects
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(6, 4, "yellow", false);

        //Square objects
        Square square1 = new Square();
        Square square2 = new Square(3, "pink", false);

        //Shape objects
        System.out.println("triangle:" + triangle);
        triangle.setColor("red");
        System.out.println("triangle:" + triangle);
        System.out.println("anotherTriangle:" + anotherTriangle);

        //Circle objects
        System.out.println("\ncircle2 area = " + circle2.getArea());
        System.out.println("circle2 perimeter = " + circle2.getPerimeter());
        System.out.println("circle1: " + circle1);
        System.out.println("circle2: " + circle2);

        //Rectangle objects
        System.out.println("\nrectangle1: " + rectangle1);
        rectangle1.setLength(9);
        rectangle1.setWidth(5);
        System.out.println("rectangle1 area = " + rectangle1.getArea());
        System.out.println("rectangle1 perimeter = " + rectangle1.getPerimeter());
        rectangle1.setColor("black");
        System.out.println("rectangle1: " + rectangle1);
        System.out.println("rectangle2 area = " + rectangle2.getArea());
        System.out.println("rectangle2: " + rectangle2);

        //Square objects
        System.out.println("\nsquare1: " + square1);
        square1.setSide(7);
        System.out.println("square1 side = " + square1.getSide());
        square1.setColor("brown");
        System.out.println("square1: " + square1);
        System.out.println("square1 area = " + square1.getArea());
        System.out.println("square2: " + square2);
        square2.setWidth(5);
        System.out.println("square2 side = " + square2.getSide());
    }
}