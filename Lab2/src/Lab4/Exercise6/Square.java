package Lab4.Exercise6;

public class Square extends Rectangle{
    Square(){
        super();
    }
    Square(double side){
        super(side, side);
    }
    Square(double side, String color, boolean filled){
        super(side, side, color, filled);
    }

    public double getSide(){
        return getLength();
    }
    public void setSide(double side){
        super.setLength(side);
        super.setWidth(side);
    }
    @Override
    public void setWidth (double side){
        System.out.println("It won't be a square anymore if you change this alone!");
        super.setLength(side);
        super.setWidth(side);
    }
    @Override
    public void setLength (double side){
        System.out.println("It won't be a square anymore if you change this alone!");
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public String toString() {
        return "A Square with side = " + getSide() + ", which is a subclass of " + super.toString();
    }
}