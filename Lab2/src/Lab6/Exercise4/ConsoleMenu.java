package Lab6.Exercise4;

import java.util.Scanner;

import static java.lang.System.in;

public class ConsoleMenu {
    public static void main(String[] args) {
        Dictionary dictionary = new Dictionary();
        int choice;
        String word, definition;

        //Note: nextLine() is a pain to use with other next() methods in scanner
        //But I don't have time to implement a better way to input from user
        //For example: to make it read a word until a 'space' so that we don't have phrases as words
        //For now we will have to trust the user to add one-word inputs for the word.
        //Also IDK if the best way of searching for a word (getDefinition() method) is by making another Word object
        //but I'll just go with it
        Scanner scanner = new Scanner(in);

        do {
            System.out.println("1. Add word\n2. Get definition\n3. Get all words\n4. Get all definitions\n5. Exit");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    System.out.println("Word (please add one-word inputs, no spaces): ");
                    word = scanner.nextLine();
                    System.out.println("Definition: ");
                    definition = scanner.nextLine();
                    dictionary.addWord(new Word(word), new Definition(definition));
                    System.out.println("Word added successfully!");
                    break;
                case 2:
                    System.out.println("Word to search for: ");
                    word = scanner.nextLine();
                    Definition result = dictionary.getDefinition(new Word(word));
                    if(result != null) {
                        System.out.println("Definition for " + word + " is: ");
                        System.out.println(result);
                    } else
                        System.out.println("Word doesn't exist");
                    break;
                case 3:
                    System.out.println("All words currently present in the dictionary: ");
                    dictionary.getAllWords();
                    break;
                case 4:
                    System.out.println("All definitions currently present in the dictionary: ");
                    dictionary.getAllDefinitions();
                case 5:
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice!");
                    break;
            }
        } while (choice != 5);
    }
}
