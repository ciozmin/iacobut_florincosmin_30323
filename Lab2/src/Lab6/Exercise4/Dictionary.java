package Lab6.Exercise4;

import java.util.HashMap;

public class Dictionary {
    private HashMap<Word, Definition> dictionary = new HashMap<>();

    public void addWord(Word w, Definition d){
        dictionary.put(w, d);
    }
    public Definition getDefinition(Word w){
        if(dictionary.containsKey(w))
            return dictionary.get(w);
        else return null;
    }
    public void getAllWords(){
        // Prints words as a list in brackets
        System.out.println(dictionary.keySet());
        // Prints each word on a new line
        //dictionary.forEach((w, d) -> System.out.println(w));
    }
    public void getAllDefinitions(){
        // Prints definitions as a list in brackets
        System.out.println(dictionary.values());
        // Prints each definition on a new line
        // dictionary.forEach((w, d) -> System.out.println(d));
    }
}
