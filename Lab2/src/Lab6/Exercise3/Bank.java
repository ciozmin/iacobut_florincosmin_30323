package Lab6.Exercise3;

import java.util.Iterator;
import java.util.TreeSet;

public class Bank {
    private TreeSet<BankAccount> accounts = new TreeSet<BankAccount>();

    public void addAccount(String owner, double balance){
        BankAccount s = new BankAccount(owner, balance);
        accounts.add(s);
    }

    public void printAccounts(){
        //print all accounts sorted by balance
        Iterator<BankAccount> i = accounts.iterator();
        System.out.println("Sorted by balance: ");
        while(i.hasNext())
            System.out.print(i.next() + "\n");
        System.out.println();
    }

    public void printAccounts(double minBalance, double maxBalance){
        //print all accounts between a min range and max range
        if (minBalance > maxBalance) minBalance += (maxBalance - (maxBalance = minBalance));
        //Iterator<BankAccount> i = accounts.subSet(minBalance, maxBalance).iterator();
        Iterator<BankAccount> i = accounts.iterator();
        BankAccount o;
        System.out.println("Accounts with balance between given range: ");
        while(i.hasNext()) {
            o = i.next();
            if(o.getBalance() >= minBalance && o.getBalance() <= maxBalance)
                System.out.print(o + "\n");
        }
        System.out.println();
    }
    public BankAccount getAccount(String owner) {
        //search for and return a BankAccount with given name
        Iterator<BankAccount> i = accounts.iterator();
        BankAccount o;
        while (i.hasNext()) {
            o = i.next();
            if (o.getOwner().equals(owner))
                return o;
        }
        return null;
    }
    public TreeSet<BankAccount> getAllAccounts(){
        return accounts;
    }
}
