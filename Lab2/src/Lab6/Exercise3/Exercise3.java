package Lab6.Exercise3;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

public class Exercise3 {
    public static void main(String[] args) {
        Bank BT = new Bank();
        BT.addAccount("Cosmin Iacobut", 250.15);
        BT.addAccount("Ionut Tirziu", 780.0);
        BT.addAccount("Razvan Botezat", 300.50);
        BT.addAccount("Sabeeh Ahmed", 520.47);
        BT.addAccount("Radu Calomfirescu", 1140);
        BT.addAccount("John Doe", 74.40);

        BT.printAccounts();
        BT.printAccounts(300, 650);
        BT.printAccounts(650, 300);

        BankAccount temp = BT.getAccount("Razvan Botezat");
        if(temp != null) System.out.println(temp);
        else System.out.println("Owner not found!");
        temp = BT.getAccount("Razvan Popa");
        if(temp != null) System.out.println(temp);
        else System.out.println("Owner not found!");

//  BASIC IMPLEMENTATION; EASIER TO UNDERSTAND BUT MORE CODE
//        TreeSet<BankAccount> accounts = new TreeSet<BankAccount>(new Comparator<>() {
//     @Override
//     public int compare(BankAccount o1, BankAccount o2) {
//         return o1.getOwner().compareTo(o2.getOwner());
//     }
// });

//  Lambda IMPLEMENTATION; A BIT HARDER TO READ BUT LESS CODE
//        TreeSet<BankAccount> accounts = new TreeSet<BankAccount>((o1, o2) -> o1.getOwner().compareTo(o2.getOwner()));

//  Comparator.comparing IMPLEMENTATION; MOST ELEGANT BUT CAN BE CONFUSING
        TreeSet<BankAccount> accounts = new TreeSet<BankAccount>(Comparator.comparing(BankAccount::getOwner));
        accounts.addAll(BT.getAllAccounts());
        Iterator<BankAccount> i = accounts.iterator();
        System.out.println("\nSorted alphabetically: ");
        while(i.hasNext())
            System.out.println(i.next());
    }
}
