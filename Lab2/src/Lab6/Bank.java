package Lab6;

import java.util.ArrayList;
import java.util.Collections;

public class Bank {
    private ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();

    public void addAccount(String owner, double balance){
        BankAccount s = new BankAccount(owner, balance);
        accounts.add(s);
    }

    public void printAccounts(){
        //print all accounts sorted by balance
        boolean sorted;
        do {
            sorted = true;
            for(int i = 0; i < accounts.size() - 1; i++) {
                if(accounts.get(i).getBalance() > accounts.get(i + 1).getBalance()) {
                    Collections.swap(accounts, i, i + 1);
                    sorted = false;
                }
            }
        } while(!sorted);

        System.out.println("Accounts sorted by balance: ");
        for(int i = 0; i < accounts.size(); i++){
            System.out.println(accounts.get(i).toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance){
        //print all accounts between a min range and max range
        if (minBalance > maxBalance) minBalance += (maxBalance - (maxBalance = minBalance));
        System.out.println("Accounts with balance between " + minBalance + " and " + maxBalance + ": ");
        for(int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getBalance() >= minBalance && accounts.get(i).getBalance() <= maxBalance) {
                System.out.println(accounts.get(i).toString());
            }
        }
    }
    public BankAccount getAccount(String owner){
        //search for and return a BankAccount with given name
        for(int i = 0; i < accounts.size(); i++) {
            if(accounts.get(i).getOwner().equals(owner)){
                return accounts.get(i);
            }
        }
        return null;
    }
    public ArrayList<BankAccount> getAllAccounts(){
        return accounts;
    }

}