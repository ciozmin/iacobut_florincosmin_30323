package Lab6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class Exercise2 {
    public static void main(String[] args) {
        Bank BT = new Bank();
        BT.addAccount("Cosmin Iacobut", 250.15);
        BT.addAccount("Ionut Tirziu", 780.0);
        BT.addAccount("Razvan Botezat", 300.50);
        BT.addAccount("Sabeeh Ahmed", 520.47);
        BT.addAccount("Radu Calomfirescu", 1140);
        BT.addAccount("John Doe", 74.40);

        BT.printAccounts();
        BT.printAccounts(300, 650);
        BT.printAccounts(650, 300);

        BankAccount temp = BT.getAccount("Razvan Botezat");
        if(temp != null) System.out.println(temp);
        else System.out.println("Owner not found!");
        temp = BT.getAccount("Razvan Popa");
        if(temp != null) System.out.println(temp);
        else System.out.println("Owner not found!");

        ArrayList<BankAccount> accounts = BT.getAllAccounts();
        accounts.sort(Comparator.comparing(BankAccount->BankAccount.getOwner()));
        System.out.println(accounts.toString());
    }
}
