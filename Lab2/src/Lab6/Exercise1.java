package Lab6;

public class Exercise1 {
    public static void main(String[] args) {
        BankAccount account1 = new BankAccount();
        BankAccount account2 = new BankAccount();

        account1.deposit(40);
        account2.deposit(120);

        if (account1.equals(account2))
            System.out.println("accounts have the same balance");
        else System.out.println("accounts don't have the same balance");

        account2.withdraw(80);
        account2.equals(account1);
        if (account2.equals(account1))
            System.out.println("accounts have the same balance");
        else System.out.println("accounts don't have the same balance");
    }
}
