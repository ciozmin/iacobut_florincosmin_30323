package Lab10.Exercise3;

public class Counter extends Thread {

    Thread thread;
    Counter(String name, Thread t){
        super(name);
        this.thread = t;
    }
    static int counter = 0;
    public void run(){

        for(int i=0;i<=100;i++){
            System.out.println(getName() + " i = "+ (counter*100 + i));
            try {
                if (thread!=null) thread.join();
                Thread.sleep((int)(100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        counter++;
    }

    public static void main(String[] args) {
        Counter c1 = new Counter("counter1",null);
        Counter c2 = new Counter("counter2",c1);
        c1.start();
        c2.start();
    }
}