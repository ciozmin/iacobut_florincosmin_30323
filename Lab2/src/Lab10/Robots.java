package Lab10;


import java.util.ArrayList;
import java.util.Random;

public class Robots{
    private int x;
    private int y;
    private int number;

    static int counter = 0;
    Robots(int x, int y){
        this.x = x;
        this.y = y;
        counter++;
        this.number=counter;
    }

    public void move(){
        Random rand = new Random();
        switch (rand.nextInt(4)){
            case 0:
                if(x < 99) x++;
                else x--;
                break;
            case 1:
                if(x > 1) x--;
                else x++;
                break;
            case 2:
                if(y < 99) y++;
                else y--;
                break;
            case 3:
                if(y > 1) y--;
                else y++;
                break;
            default:
                break;
        }
        System.out.println(this.number + " " + this.x + "," + this.y);
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public static void main(String[] args) {
        Random rand = new Random();
        Robots r1 = new Robots(rand.nextInt(100),rand.nextInt(100));
        Robots r2 = new Robots(rand.nextInt(100),rand.nextInt(100));
        Robots r3 = new Robots(rand.nextInt(100),rand.nextInt(100));
        Robots r4 = new Robots(rand.nextInt(100),rand.nextInt(100));
        Robots r5 = new Robots(rand.nextInt(100),rand.nextInt(100));
        Robots r6 = new Robots(rand.nextInt(100),rand.nextInt(100));
        Robots r7 = new Robots(rand.nextInt(100),rand.nextInt(100));
        Robots r8 = new Robots(rand.nextInt(100),rand.nextInt(100));
        Robots r9 = new Robots(rand.nextInt(100),rand.nextInt(100));
        Robots r10 = new Robots(rand.nextInt(100),rand.nextInt(100));


        Map map = new Map();
        map.addRobot(r1);
        map.addRobot(r2);
        map.addRobot(r3);
        map.addRobot(r4);
        map.addRobot(r5);
        map.addRobot(r6);
        map.addRobot(r7);
        map.addRobot(r8);
        map.addRobot(r9);
        map.addRobot(r10);

        map.start();


    }

}
class Map extends Thread {
    ArrayList<Robots> r = new ArrayList<Robots>();

    public void addRobot(Robots r1){
        this.r.add(r1);
    }

    public void run() {
        while (r.size() >= 2) {
            for (int i = 0; i < r.size() - 1; i++) {
                synchronized (r) {
                    r.get(i).move();
                    try {
                        sleep(100);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            for (int i = 0; i < r.size() - 1; i++) {
                for (int j = i + 1; j < r.size(); j++) {
                    if (r.get(i).getX() == r.get(j).getX() && r.get(i).getY() == r.get(j).getY()) {
                        synchronized (r) {
                            r.remove(j);
                            r.remove(i);
                            try {
                                sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if(r.size()<2)
                        {
                            return;
                        }
                        i = 0;
                        j = i + 1;
                    }
                }
            }
        }
    }

}