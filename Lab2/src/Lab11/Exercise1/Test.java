package Lab11.Exercise1;

import java.awt.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.*;

public class Test extends JFrame{

    Test(View view) {
        setLayout(new BorderLayout());
        add(view,BorderLayout.NORTH);
        pack();
        setVisible(true);
    }
    public static void main(String[] args) {
        Model model = new Model();
        model.start();

        View view = new View();
        Controller controller = new Controller(model,view);

        new Test(view);
    }
}
