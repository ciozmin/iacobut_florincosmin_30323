package Lab11.Exercise1;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class View extends JPanel implements Observer {

    JLabel label;
    JTextField textField;

    View(){
        this.setLayout(new FlowLayout());
        textField = new JTextField(20);
        label = new JLabel("Temperature");
        add(textField);add(label);
    }

    @Override
    public void update(Observable o, Object arg) {
        String s = ""+((Model)o).getTemp();
        textField.setText(s);
    }
}