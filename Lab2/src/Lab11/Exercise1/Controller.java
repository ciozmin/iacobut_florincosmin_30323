package Lab11.Exercise1;

public class Controller {
    Model m;
    View v;
    public Controller(Model m, View v) {
        m.addObserver(v);
        this.m = m;
        this.v = v;
    }
}