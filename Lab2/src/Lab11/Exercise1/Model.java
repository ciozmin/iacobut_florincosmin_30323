package Lab11.Exercise1;

import java.util.Observable;
import java.util.Random;

public class Model extends Observable implements Runnable {

    Random r = new Random();
    Thread t;
    double temp = 20.0;
    private boolean active = true;
    private boolean paused = false;

    public void start(){
        if(t==null){
            t = new Thread(this);
            t.start();
        }
    }

    public void run(){
        while(active){
            if(paused){
                synchronized(this){
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }//.sync
            }//.if

            int d = r.nextInt(5);
            int x = r.nextInt(30);
            if(x<15) d = -1*d;

            if(temp + d < 100 && temp + d > 0){
                temp = temp + d;
                this.setChanged();
                this.notifyObservers();
            }

            try {Thread.sleep(1000);} catch (InterruptedException e) {}
        }//.while
    }//.run

    public double getTemp() {
        return temp;
    }

}