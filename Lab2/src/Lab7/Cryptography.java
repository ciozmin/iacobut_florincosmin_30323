package Lab7;

import java.io.*;

public class Cryptography {
    private FileReader file01, file02;

    Cryptography(FileReader file1, FileReader file2) throws IOException {
        this.file01 = file1;
        this.file02 = file2;
    }

    private String readFile(boolean encrypt) throws IOException {
        String srcString;
        if(encrypt) {
            try (BufferedReader br = new BufferedReader(file01)) {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    //sb.append(System.lineSeparator());
                    sb.append('\n');
                    line = br.readLine();

                }
                srcString = sb.toString();
            }

            char[] result = new char[srcString.length()];

            for (int i = 0; i < result.length; i++) {
                if (srcString.charAt(i) != '\n')
                    result[i] = (char) (srcString.charAt(i) + 1);
                else
                    result[i] = (srcString.charAt(i));
            }
            String destString = new String(result);
            return destString;
        } else {
            try (BufferedReader br = new BufferedReader(file02)) {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    //sb.append(System.lineSeparator());
                    sb.append('\n');
                    line = br.readLine();

                }
                srcString = sb.toString();
            }

            char[] result = new char[srcString.length()];

            for (int i = 0; i < result.length; i++) {
                if (srcString.charAt(i) != '\n')
                    result[i] = (char) (srcString.charAt(i) - 1);
                else
                    result[i] = (srcString.charAt(i));
            }
            String destString = new String(result);
            return destString;
        }
    }

    private void encryptFile(String data) throws IOException {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\data.enc"), "utf-8"))) {
            writer.write(data);
        }
    }

    private void decryptFile(String data) throws IOException {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\data.dec"), "utf-8"))) {
            writer.write(data);
        }
    }

    public static void main(String[] args) throws IOException {
        FileReader file01 = new FileReader("D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\data");
        FileReader file02 = new FileReader("D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\data.enc");
        Cryptography test = new Cryptography(file01, file02);
        test.encryptFile(test.readFile(true));
        test.decryptFile(test.readFile(false));
    }
}