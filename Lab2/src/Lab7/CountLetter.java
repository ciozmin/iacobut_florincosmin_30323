package Lab7;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CountLetter {
    private final char lookFor;
    private FileReader file01;

    CountLetter(FileReader file, char lookFor) throws IOException {
        this.file01 = file;
        this.lookFor = lookFor;
    }

    private String readFile() throws IOException {

        String everything;

        try (BufferedReader br = new BufferedReader(file01)) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();

            }
            everything = sb.toString();
        }
        return everything;
    }

    private int count(String line) {
        int count = 0;

        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == lookFor) {
                count++;
            }
        }

        return count;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Char to be read from file: ");
        char c = (char)br.read();

        FileReader file01 = new FileReader("D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\exercise2");
        CountLetter cl1 = new CountLetter(file01, c);

        System.out.println("'" + c + "' appears " + cl1.count(cl1.readFile()) + " times.");
    }

}