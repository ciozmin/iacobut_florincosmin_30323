package Lab7;

import java.io.*;

public class Car implements Serializable {
    private String model;
    private double price;

    public Car(){
        this.model = null;
        this.price = 0.0;
    }
    public Car(String model, double price){
        this.model = model;
        this.price = price;
    }

    public void honk(){
        System.out.println("HONK!");
    }
    public double getPrice() {
        return price;
    }
    public String getModel() {
        return model;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    public void setModel(String model) {
        this.model = model;
    }

    void saveCar(Car a, String storeRecipientName) throws IOException {
        ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(storeRecipientName));
        o.writeObject(a);
        System.out.println(a + " : car object saved.");
    }

    Car loadCar(String storeRecipientName) throws IOException, ClassNotFoundException{
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(storeRecipientName));
        Car x = (Car)in.readObject();
        System.out.println(x + " : car object loaded.");
        return x;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }

    //Need to work on this more.
    //Should implement a list that loads each of them. At the moment it only loads the last object saved
    // + you can't view the objects that are saved
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Car car1 = new Car("Honda Civic", 22350.0);
        Car car2 = new Car("Toyota Camry",25295.0);
        Car car3 = new Car("Ford Mustang",27205.0);

        System.out.println(car1.toString());
        car1.saveCar(car1,"D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\cars.dat");
        car2.saveCar(car2,"D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\cars.dat");
        car3.saveCar(car3,"D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\cars.dat");

        Car carx = car1.loadCar("D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\cars.dat");
        Car cary = car2.loadCar("D:\\Projects\\Bitbucket SE Lab\\Lab2\\src\\Lab7\\cars.dat");
        System.out.println(carx);
        System.out.println(cary);
    }
}
