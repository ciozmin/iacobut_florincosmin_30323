package Lab7;

public class CoffeeTest {
    public static void main(String[] args) throws CoffeesException {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for (int i = 0; i < 15; i++) {
            Coffee c = mk.makeCoffee();
            try {
                d.drinkCoffee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception: " + e.getMessage() + " temp = " + e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception: " + e.getMessage() + " conc = " + e.getConc());
            } finally {
                System.out.println("Throw the coffee cup.\n");
            }
        }
    }
}//.class

class CoffeeMaker {
    Coffee makeCoffee() throws CoffeesException{
        System.out.println("Make a coffee");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee coffee = new Coffee(t,c);
        if(coffee.getCount()>5)
            throw new CoffeesException(coffee.getCount(),"Too many coffees!");
        return coffee;
    }

}//.class

class Coffee {
    private int temp;
    private int conc;
    private static int count = 0;

    Coffee(int t, int c){
        temp = t;
        conc = c;
        count++;
    }
    int getTemp(){
        return temp;
    }
    int getConc(){
        return conc;
    }
    int getCount() {
        return count;
    }
    public String toString(){return "[coffee temperature="+temp+":concentration="+conc+"]";}
}//.class

class CoffeeDrinker{
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException {
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Coffee is too hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Coffee concentration too high!");
        System.out.println("Drink coffee:"+c);
    }
}//.class

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t, String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp(){
        return t;
    }
}//.class

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c, String msg) {
        super(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }
}//.class
class CoffeesException extends Exception{
    int n;
    public CoffeesException(int n, String msg) {
        super(msg);
        this.n = n;
    }

    int getCount(){
        return n;
    }
}//.class